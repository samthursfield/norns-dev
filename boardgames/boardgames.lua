-- boardgames: WIP
--

bpm = 120.0
-- loop length must be under 5 mins...
-- for now we assume 512 beats at 120bpm
-- which is 256seconds.
loop_length = 512.0 * (60.0 / bpm)

play = 1

rate = 1.0
rec = 1.0
pre = 0.0
dry_level = 1.0

g = grid.connect()

function init()
  -- route audio input to softcut input
  audio.level_adc_cut(1)
  audio.level_monitor(dry_level)

  softcut.buffer_clear()

  -- activate voice 1 and link to buffer 1
  softcut.enable(1, 1)
  softcut.buffer(1, 1)

  softcut.level(1, 1.0)
  softcut.rate(1, rate)

  -- start our long-ass loop.
  softcut.loop(1, 1)
  softcut.loop_start(1, 1)
  softcut.loop_end(1, loop_length)

  -- let's go!
  softcut.position(1, 1)
  softcut.play(1, 1)

  -- set input rec level: input channel, voice, level
  softcut.level_input_cut(1, 1, 1.0)
  softcut.level_input_cut(2, 1, 1.0)
  -- set voice 1 record level
  softcut.rec_level(1, rec)
  -- set voice 1 pre level
  softcut.pre_level(1, pre)
  -- set record state of voice 1 to 1
  softcut.rec(1,1)

  -- start position updates
  local update_sleep_time = 60.0 / bpm / 16.0
  softcut.phase_quant(1, update_sleep_time)
  softcut.event_phase(grid_draw_playhead)
  softcut.poll_start_phase()
end

function enc(n,d)
  if n==1 then
    rate = util.clamp(rate+d/100,-4,4)
    softcut.rate(1,rate)
  elseif n==2 then
    rec = util.clamp(rec+d/100,0,1)
    softcut.rec_level(1,rec)
  elseif n==3 then
    pre = util.clamp(pre+d/100,0,1)
    softcut.pre_level(1,pre)
  end
  redraw()
end

function key(n,z)
  if n==2 and z==1 then
    if dry_level == 1.0 then dry_level = 0.0 else dry_level = 1.0 end
    audio.level_monitor(dry_level)
  elseif n==3 and z==1 then
    if pre==1 then pre = 0 else pre = 1 end
    softcut.pre_level(1,pre)
  end
  redraw()
end

g.key = function(x,y,z)
  print(x,y,z)
  if y == 5 and x == 1 and z == 0 then
    --if play == 1 then play = 0 else play = 1 end
    --print("Start/stop playback: state ", play)
    if rate ~= 0 then rate = 0 else rate = 1.0 end
    softcut.rate(1, rate)
  end

  if y == 8 then
    if z == 1 then
      -- bottom row is 16 beats per step (8 seconds at 120bpm)
      local low_step = 16 * (60 / bpm)
      local sample_pos = (x - 1) * low_step
      softcut.position(1, sample_pos)
      softcut.play(1, 1)
      print("Set position: ", sample_pos)
    else
      softcut.play(1, 0)
    end
  end
  g:led(x,y,z*15)
  g:refresh()
end

function redraw()
  screen.clear()
  screen.move(10,10)
  screen.text("board games.")
  screen.move(10,30)
  screen.text("rate: ")
  screen.move(118,30)
  screen.text_right(string.format("%.2f",rate))
  screen.move(10,40)
  screen.text("rec: ")
  screen.move(118,40)
  screen.text_right(string.format("%.2f",rec))
  screen.move(10,50)
  screen.text("feedback: ")
  screen.move(118,50)
  screen.text_right(string.format("%.2f",pre))
  screen.move(10,60)
  screen.text("dry: ")
  screen.move(118,60)
  screen.text_right(string.format("%.2f",dry_level))
  screen.update()
end

function grid_draw_playhead(voice, position)
  if voice == 1 then
    local low = math.floor(position / 8.0) % 16 + 1
    local mid = math.floor(position * 2) % 16 + 1
    local high = math.floor(position * 16) % 16 + 1
    g:all(0)
    g:led(high, 6, 4)
    g:led(mid, 7, 4)
    g:led(low, 8, 4)
    g:refresh()
  end
end
